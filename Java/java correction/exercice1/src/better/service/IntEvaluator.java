package better.service;

public class IntEvaluator {
    public String evaluateValueFor(int value) {
        return String.format(
                "Le nombre est %s %s",
                isNegativeOrPositive(value),
                isOddOrEven(value)
                );
    }

    private String isOddOrEven(int value) {
        String result;
        if(value == 0) {
            result = "(et il est pair)";
        } else {
            int modulo = value % 2;
            result = modulo == 0 ? "et pair" : "et négatif";
        }
        return result;
    }

    private String isNegativeOrPositive(int value) {
        String positiveOrZero = value == 0 ? "zero" : "positif";
        return (value >= 0) ? positiveOrZero : "negatif";
    }
}
