package better;

import better.service.IntEvaluator;

import java.util.Scanner;

public class PairImpair {

    private static IntEvaluator intEvaluator = new IntEvaluator();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Entrez un nombre entier:");
        while (!scanner.hasNextInt()) {
            System.out.println("Merci de bien vouloir fournir un entier valide");
            scanner.next();
        }
        System.out.println(intEvaluator.evaluateValueFor(scanner.nextInt()));
    }

}
