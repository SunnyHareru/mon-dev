import java.io.File;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //Déclaration des variables
        Scanner sc = new Scanner(System.in);
        System.out.println("Please choose a path :");
        String str = sc.nextLine();
        File f=new File(str);

        //Exercice 3 Informations sur le fichier/dossier
        if (f.exists()) {
            System.out.println(f.getAbsolutePath());
            System.out.println("It exists ? " + f.exists());
            System.out.println("Is that a Directory ? " + f.isDirectory());
            System.out.println("Is that a file ? " + f.isFile());


        // Exercice 1 Listing des fichiers dans le dossier sélectionné
        String[] list=f.list();
        for (int i=0; i<list.length; i++) {
            File explorerlist=new File(list[i]);
            if (explorerlist.isDirectory()) System.out.println("Dossier \t"+list[i]);
            else System.out.println(""+explorerlist.length()+" \t"+list[i]);
        }

        }
           else {
            System.out.println("The directory/file doesn't exist");
        }
    }
}
