import java.io.*;
import java.net.URL;
import java.util.Scanner;


public class Main {

    public static void saveImage(String imageLink, String destinationFile, String name) throws IOException {
        //Déclaration des variables
        URL url = new URL(imageLink);
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(new File(destinationFile,name));

        byte[] b = new byte[4096];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
    }

    public static void main(String[] args) throws IOException {
        //Informations relatives à l'image que l'on récupére
        Scanner sc = new Scanner(System.in);
        System.out.println("Write your save destination :");
        String destination = sc.nextLine();

        System.out.println("Write name and extension of your picture :");
        String extension = sc.nextLine();

        System.out.println("Picture link :");
        String link = sc.nextLine();
        // Récupération de l'image + Gestion exception
        try {
            saveImage(destination,extension,link);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}