import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws IOException {

        //Récupération des informations de l'utilisateur
        Scanner scan = new Scanner(System.in);
        System.out.println("Write your new file name (with extension) :");
        String namefile = scan.nextLine();
        File file = new File(namefile);
        FileWriter fos = new FileWriter(file);
        String newline = System.getProperty("line.separator");
        try{
            System.out.println("Write your text (write \"quit\" to stop) : ");
            String ligne = " ";

            // boucle continue jusqu'au "quit" + retour à la ligne
            while(!ligne.equals("quit")){
                ligne = scan.nextLine();
                fos.write(ligne);
                fos.write(newline);

            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally{
            try{
                if (fos !=null)
                    fos.close();
            }
            catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}