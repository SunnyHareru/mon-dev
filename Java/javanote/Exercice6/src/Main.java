import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        FileInputStream fis = null;
        FileOutputStream fos = null;

        try {
            //Récupération du fichier à copier vers le nouveau fichier
            fis = new FileInputStream(new File("texte.txt"));
            fos = new FileOutputStream(new File("texte2.txt"));

            byte[] buff = new byte[16];
            //boucle de lecture du fichier texte.txt, tant qu'il y a des caractères ça continue
            while (fis.read(buff) >= 0) {
                fos.write(buff);
                buff = new byte[8];

            }
            System.out.println("Copy done");
            }
            //Gestion des exceptions
        catch (FileNotFoundException e) {
            e.printStackTrace();}

        catch (IOException e) {
            e.printStackTrace(); }

        finally {

            try {
                if (fis != null)
                    fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (fos != null)
                    fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}