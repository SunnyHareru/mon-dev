import java.io.*;
import java.util.*;
import java.lang.*;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Please choose a path :");
        String strpath = sc.nextLine();

        System.out.println("Please choose a file extension (say 'no' to quit) :");
        String strext = sc.nextLine();
        File extension = new File(strext);

        File[] files = new File (extension);
        for (int i = 0; i < files.length; i++) {
            File explorerlist = new File(files[i]);
            if (files[i].isFile() == true) {
                String nomFichier = files[i].getName().toLowerCase();
                if (nomFichier.endsWith(strext)) {
                    System.out.println("Dossier \t" + files[i]);
                } else System.out.println("" + explorerlist.length() + " \t" + files[i]);
            }
        }
    }
}
